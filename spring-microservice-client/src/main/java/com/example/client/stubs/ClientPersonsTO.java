package com.example.client.stubs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomask79 on 13.11.16.
 */
public class ClientPersonsTO {
    public List<ClientPersonTO> persons = new ArrayList<ClientPersonTO>();

    public List<ClientPersonTO> getPersons() {
        return persons;
    }

    public void setPersons(List<ClientPersonTO> persons) {
        this.persons = persons;
    }

}
