# Writing MicroServices [part 6] #

## Spring Cloud MicroServices in Docker (Docker Swarm) ##

Okay, after I showed howto make Spring-Cloud MicroService topology running in Docker via:

* [Docker port mapping to the outer host network](https://bitbucket.org/tomask79/microservice-spring-cloud-docker) 
* [Docker links](https://bitbucket.org/tomask79/microservice-spring-cloud-docker2)
* [Docker Bridge Network](https://bitbucket.org/tomask79/microservice-spring-cloud-docker3)
 
It's time to get the hands dirty on the Docker Cloud.

### Prerequisities for the demo ###

* **Two separated virtual machines in the Microsoft Azure Cloud** on the same subnet. Let's call them **Node1(172.20.122.4)** and **Node2(172.20.122.7)**.
* **Ubuntu** on both nodes
* **Git** on both nodes
* **Maven** on both nodes
* **On both nodes** builded registry, service and client images from previous parts

### Spring Cloud components Images preparation ###

On both nodes please run the following:

* git clone <this repo>
* In the **spring-microservice-registry** folder run:
```
mvn clean install docker:build
```
* In the **spring-microservice-service** folder run:
```
mvn clean install docker:build
```
* In the **spring-microservice-client** folder run:
```
mvn clean install docker:build
```
* **docker images** to verify that **registry/demo, service/demo and client/demo** images are ready!

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
client/demo         latest              cd45c5340541        38 minutes ago      351.2 MB
service/demo        latest              24b20a6ecddb        55 minutes ago      347.8 MB
registry/demo       latest              176b116b275b        22 hours ago        352.4 MB
java                8-jre               07e7184b1bb1        12 days ago         311.3 MB
```


### Docker Swarm Cloud preparation ###

On Node1:
```
docker swarm init
```
since this moment, node1 is the master node. Let's join node2 to it as worker node:

```
docker swarm join \
    --token SWMTKN-1-035lc1gb8oqtv16f372uozlfindje887k4mu9wbyenu8hxn0dz-52vflnf0x2rbbr8a24pdyj87v \
    172.20.122.4:2377
```
Please keep in mind that token and master address is going to be different in your case. Now create [Docker OverLay](https://docs.docker.com/engine/userguide/networking/get-started-overlay/) network to connect future services at these two nodes.

```
docker network create -d overlay spring-cloud-network
```
Verify that network is created okay with:

```
docker network ls
```
Output should be:
```
NETWORK ID          NAME                   DRIVER              SCOPE
5ec63f695752        bridge                 bridge              local
4dd4122c0f37        docker_gwbridge        bridge              local
c0dbbf1e7584        host                   host                local
70aeejrz60bq        ingress                overlay             swarm
b6e9f74407d8        none                   null                local
4of1i36phgkw        spring-cloud-network   overlay             swarm
```

All right, now let's deploy all Spring-Cloud components into Overlay network as [services](https://docs.docker.com/engine/reference/commandline/service_create/). 

**Eureka service**:

**Node1:**
```
docker service create --name eureka --network spring-cloud-network --publish 9761:9761 --replicas 2 registry/demo
```
verify that eureka service is really running with 2 replicas on both Azure nodes:

```
docker service ps eureka
```
output should be:

```
ID                         NAME      IMAGE          NODE           DESIRED STATE  CURRENT STATE         ERROR
6q7bsk8n2k5ae3l7oss8lhg3c  eureka.1  registry/demo  cmwork2docker  Running        Running 22 hours ago
32v7x9lpwy9ei881a7l2bfykb  eureka.2  registry/demo  cmwork1docker  Running        Running 22 hours ago
```
verify from your computer that eureka netflix server is running at **http://node1:9761**

**Deploying the MicroService into Docker Azure Swarm Cloud**:

**Node1:**
```
docker service create --name service --network spring-cloud-network --replicas 2 service/demo
```
verify that MicroService runs really in two replicas:

```
docker service ps service
```
output should be:

```
ID                         NAME       IMAGE         NODE           DESIRED STATE  CURRENT STATE              ERROR
4fe52kw02t2sha6nz5mwidhwh  service.1  service/demo  cmwork1docker  Running        Running about an hour ago
2rz09i82x095wo9qlzgzto6qt  service.2  service/demo  cmwork2docker  Running        Running about an hour ago
```

**Deploying MicroService client into Azure Docker Cloud:**

**Node1:**
```
docker service create --name client --network spring-cloud-network --replicas 2 client/demo
```
verify that client runs in two replicas (docker service ps client):

```
ID                         NAME      IMAGE        NODE           DESIRED STATE  CURRENT STATE          ERROR
2iggh9cde38hrmvhzjqimc7u9  client.1  client/demo  cmwork1docker  Running        Running 3 seconds ago
8jik6ix59owcq2tzvmqrzzizp  client.2  client/demo  cmwork2docker  Running        Running 3 seconds ago
```
Now attach to the running client:

**At Node1 or Node2** run:
```
docker attach <client container ID, obtain ID via "docker ps">
```

You should see again repeated output:

```
*************************************************************
Simple MicroService Feign invocation result :{"persons":[{"name":"Tomas","surname":"Kloucek","department":"Programmer"}]}
*************************************************************
```
### (Summary) Things you REALLY want to know about Docker Swarm clustering ###

* **By default, service discovery assigns a virtual IP address (VIP) and DNS entry to each service in the swarm, making it available by its service name to containers on the same network.** This is very usefull for Spring Cloud components, notice how I named all services by --name (eureka, service and client), because of that all components are reachable within the cluster by just eureka, service or client host names!

* The swarm load balancer automatically routes the HTTP request to the service’s VIP to an active task. It distributes subsequent requests to other tasks **using round-robin selection**. I love this! Anyway, this Docker Swarm feature is making Ribbon Spring Cloud load balancer little ambigous.:)

Enjoy the Swarm! Well, we'll be using it in the company where I work soon even in the production...:)

see you

Tomas